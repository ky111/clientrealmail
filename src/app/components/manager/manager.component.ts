import { Component, OnInit } from '@angular/core';
import { WhiteList } from 'src/app/classes/white-list';
import { ListManagerService } from 'src/app/services/list-manager.service';
// const ELEMENT_DATA:   WhiteList[] = [
//   {id:1,IPAddress:"123456789",DescriptionAbout:"lalala"},//,isEdit:false},
//   {id:2,IPAddress:"123456789",DescriptionAbout:"lalala"},//,isEdit:false},
//   {id:3,IPAddress:"123456789",DescriptionAbout:"lalala"},//,isEdit:false},
//   {id:4,IPAddress:"123456789",DescriptionAbout:"lalala"},//,isEdit:false},
//   {id:5,IPAddress:"123456789",DescriptionAbout:"lalala"},//,isEdit:false},
// ];
@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})

export class ManagerComponent implements OnInit {

  constructor(private lmSer:ListManagerService) { }

  dataSource:Array<WhiteList>=[]
  ipEdit:string
  desEdit:string
  displayedColumns: string[] = ['id', 'IPAddress', 'DescriptionAbout','Edit','Delete'];
  wl:WhiteList=new WhiteList();

  addIP(){
    this.lmSer.addIP(this.wl).subscribe(
      d=>{console.log(d)
        // this.ngOnInit()
        this.wl.id=d[0]
        // var x=new WhiteList()
        // x.IPAddress=this.wl.IPAddress
        // x.DescriptionAbout=this.wl.DescriptionAbout
        // x.isEdit=false
        // x.id=d[0]
        this.dataSource.push(this.wl)
        this.wl.IPAddress=""
        this.wl.DescriptionAbout=""
      
      },e=>{console.log(e.message)}
    )
  }
  UpdateIP(row)
  {
    row.isEdit=true
    this.ipEdit=row.ipAddress
    this.desEdit=row.descriptionAbout
  }
  UpdateIPServer(row:WhiteList)
  {
    debugger
    var wl=new WhiteList()
    wl.id=row.id;wl.IPAddress=this.ipEdit;wl.DescriptionAbout=this.desEdit
    this.lmSer.UpdateIP(wl).subscribe(
      d=>{console.log(d)
        // this.ngOnInit()

        row.isEdit=false
        var editObj=this.dataSource.find(x=>x.id==row.id)
        editObj.IPAddress=row.IPAddress
        editObj.DescriptionAbout=row.DescriptionAbout
      },e=>{console.log(e.message)}
    )
  }
  delIP(id:number)
  {
    console.log(this.dataSource)
    this.lmSer.DeleteIP(id).subscribe(
      d=>{console.log(d);this.ngOnInit()
      for(var i=0;i<this.dataSource.length;i++)
      {
        if(this.dataSource[i].id==id)
        {
          this.dataSource.splice(i,1)
          console.log(this.dataSource)
          break
        }
      }
      },e=>{console.log(e.message)}
    )
  }
  ngOnInit() {
    this.lmSer.get().subscribe(
      d=>{this.dataSource=d
        console.log(d)
          console.log(this.dataSource[0])         
      },
      e=>{console.log(e)}
    )
    
  }

}