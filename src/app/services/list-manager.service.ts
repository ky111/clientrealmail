import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WhiteList } from '../classes/white-list';

@Injectable({
  providedIn: 'root'
})
export class ListManagerService {

  constructor(private http:HttpClient) { }
  
  URL:string="/api/whiteList/"
  // URL:string="services/api/whiteList/"


// URL:string="https://localhost:44331/api/whiteList/"
get():Observable<Array<WhiteList>>
{
  return this.http.get<Array<WhiteList>>(this.URL+"getWhiteList")
}
  
  addIP(whiteList:WhiteList):Observable<number>{
    return this.http.post<number>(this.URL+"addIP",whiteList)
  }

  // addIP(whiteList:WhiteList):Observable<void>{
  //   return this.http.post<void>(this.URL+"addIP",whiteList)
  // }
  UpdateIP(whiteList:WhiteList):Observable<WhiteList>
  {
    return this.http.post<WhiteList>(this.URL+"updateIP",whiteList)
  }
  DeleteIP(id:number):Observable<void>
  {
    return this.http.get<void>(this.URL+"deleteIP?id="+id)
  }
}
