import { TestBed } from '@angular/core/testing';

import { TryService } from './try.service';

describe('TryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TryService = TestBed.get(TryService);
    expect(service).toBeTruthy();
  });
});
